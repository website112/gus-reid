---
layout: videos
title: Classes
---

## Body-Being Principles:

In my principle-based classes I teach the principles that inform what it is to be a body. It takes a little work at first to build them up, but once they start to "click" you'll realise that this is how life should feel.
We use imagery and feeling-states to bring about alignments in the body that the conscious mind alone would find too complex to manage.
If you keep coming to my classes, and keep studying, practicing and contemplating the principles and points I'm teaching, you will transform your experience of your body and your life. But it's on you.
This kind of teaching is not for everyone. It requires you to put some skin in the game. To let go of how you've been doing things and to step into a new way of relating to your body. If you want to be told what to do and how to move, then you will get little from these classes. If, however, you come with an open mind and heart, and a willingness to experiment with the work, then you are in a great place for a breakthrough experience - one that can be lifechanging in a deep and lasting way.
The principles I teach are not arbitrary, they are grounded in reality. And if one surrenders their body-mind to them they will find themselves aligning to the natural state of the organism; one that is in harmony with nature and life.

## Relaxation and Intrinsic Pleasure:

My intention with these classes is to help you facilitate a connection to the innate intelligence of you body. Each of us has an inner guidance system. And I have found that Intrinsic Pleasure is a gateway to becoming intimate with that wisdom. This is a time to slow down, to get in touch with our bodies and feel deeply.
We may use movements to help release tension from body-parts that commonly hold on. But mostly we will focus on getting in touch with Intrinsic Pleasure and allowing it to guide our movements freely.
